import re
from pathlib import Path
from setuptools import setup, find_packages

txt = Path('ctab/__init__.py').read_text()
version = re.search("__version__ = '(.*)'", txt).group(1)

long_description = Path('README.rst').read_text()

setup(name='ctab',
      version=version,
      description='Crontab replacement',
      long_description=long_description,
      author='J. J. Mortensen',
      author_email='jjmo@dtu.dk',
      url='https://gitlab.com/jensj/ctab',
      packages=find_packages(),
      entry_points={'console_scripts': ['ctab = ctab.cli:main']},
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: '
          'GNU General Public License v3 or later (GPLv3+)',
          'Operating System :: Unix',
          'Programming Language :: Python :: 3 :: Only',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7'])
# 'Topic :: Text Editors :: Text Processing'  # ???
