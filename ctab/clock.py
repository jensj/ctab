import pickle
import socket
from pathlib import Path


class Clock:
    def __init__(self):
        self.dir = Path.home() / '.ctab'

    def start(self):

        addr = self.dir / 'socket'
        with socket.socket(socket.AF_UNIX) as s:
            s.bind(str(addr))
            s.listen()
            while True:
                conn, _ = s.accept()
                with conn:
                    command, _, args = conn.recv(1024).decode().partition(' ')
                    assert conn.recv(1024) == b''
                    if command == 'stop':
                        break
                    if command == 'ping':
                        reply = 'pong'
                    elif command == 'run':
                        reply = self.run(args)
                    conn.sendall(pickle.dumps(reply, pickle.HIGHEST_PROTOCOL))
        addr.unlink()

    def run(self, name):



def communicate(command):
    addr = Path.home() / '.ctab/socket'
    with socket.socket(socket.AF_UNIX) as s:
        s.connect(str(addr))
        s.send(command.encode())
        if command != 'stop':
            return pickle.loads(b''.join(iter(lambda: s.recv(8096), b'')))
