=======
MyQueue
=======

Simple frontend for SLURM_ and PBS_.

* Documentation: https://myqueue.readthedocs.io/
* Code: https://gitlab.com/jensj/myqueue/

.. _SLURM: https://slurm.schedmd.com/
.. _PBS: http://www.pbspro.org/
