import argparse
import sys
from pathlib import Path


class CTABError(Exception):
    """For nice (expected) CLI errors."""


main_description = """\
Simple crontab replacement.

Type "ctab help [<command>]" for help.
See https://gitlab.com/jensj/ctab/README.rst for more information.
"""


table_template = '''\
"""Table of tasks for ctab."""

table = [
    {'name': 'example',
     # 'time': '*:15',
     'commands': 'echo "Hello"'}]
'''


def main() -> None:
    parser = argparse.ArgumentParser(
        prog='ctab',
        description=main_description)

    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='More output.')
    parser.add_argument('-q', '--quiet', action='count', default=0,
                        help='Less output.')

    subparsers = parser.add_subparsers(title='Commands', dest='command')

    p = subparsers.add_parser('start',
                              description='Start',
                              help='start')

    p = subparsers.add_parser('run',
                              description='Start',
                              help='start')
    p.add_argument('name')

    p = subparsers.add_parser('completion',
                              description='Do this:  '
                              '"ctab completion >> ~/.bashrc".',
                              help='Set up tab-completion for Bash.')

    args = parser.parse_args()

    verbosity = 1 - args.quiet + args.verbose  # 0, 1 or 2

    # Create ~/.ctab/table.py if it's not there:
    dir = Path.home() / '.ctab'
    if not dir.is_dir():
        dir.mkdir()
    tab = dir / 'table.py'
    if not tab.is_file():
        tab.write_text(table_template)
        if verbosity:
            print(f'Created {tab} for you.')

    if args.command == 'completion':
        filename = Path(__file__).with_name('complete.py')
        cmd = f'complete -o default -C "{sys.executable} {filename}" mq'

        if args.verbose:
            print('Add tab-completion for Bash by copying the following '
                  f'line to your ~/.bashrc (or similar file):\n\n   {cmd}\n')
        else:
            print(cmd)

    elif args.command == 'start':
        from .clock import Clock
        Clock().run()

    else:
        from .clock import communicate

        if args.command == 'stop':
            communicate('stop')

        elif args.command == 'run':
            communicate('run ' + args.name)

        else:
            1 / 0
